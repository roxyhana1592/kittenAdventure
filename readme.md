Perdonad que nos retrasemos, crear un tutorial lleva bastante tiempo pero os lo subiremos en la carpeta indicada a continuación.

[Carpeta compartida en Google Drive](https://drive.google.com/open?id=1ComlvSI6ME-vz8EHY-aK8L9ABY0lQuQg)

# Guía rápida 

- Descargar el archivo Godot_v3.0.4-stable_win64.exe en vuestros ordenadores.
- Descargar la carpeta kittenAdventure.
- Guardar ambas cosas en una carpeta llamada Godot (donde queráis, puede ser en escritorio, mis documentos….)
- Abrimos el documento pdf (TUTORIAL GODOT - MI PRIMER VIDEOJUEGO) si queremos crear nuestro juego desde cero.
- Ejecutamos el archivo Godot_v3.0.4-stable_win64.exe 
- Se abrirá el programa. Vamos a pinchar en Scan, para buscar la carpeta kittenAdventure.
- Abrimos el juego.
- Podemos ver el código y jugar.
